const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const users = require('./users.json');

app.use('/chapter3',express.static('challange_chapter3'));
app.use('/chapter4',express.static('challange_chapter4'));

app.get('/chapter3', function(req,res){
	res.sendFile(__dirname + '/challange_chapter3/game.html');
});

app.get('/chapter4', function(req,res){
	res.sendFile(__dirname + '/challange_chapter4/main.html');
});


// Get data Users.json
app.get('/users', function(req,res){
	// Function untuk membuat tampilan ascending berdasarkan nama
	function compare(a,b){
		const nama1 = a.name.toUpperCase();
		const nama2 = b.name.toUpperCase();
	

	let perbandingan = 0;
	if(nama1 > nama2){
		perbandingan = 1
	} else if (nama1 < nama2){
		perbandingan = -1;
	}
	return perbandingan;

} 
	res.status(200).json(users.users.sort(compare)); 

});


app.listen(port, function(){
    console.log(`Server sedang berjalan di ${port}`);
});