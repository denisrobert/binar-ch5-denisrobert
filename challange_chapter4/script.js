const batu_user = document.getElementById("batu");
const kertas_user = document.getElementById("kertas");
const gunting_user = document.getElementById("gunting");
const papan_hasil = document.getElementById("hasil");
const refresh_button = document.getElementById("refresh-btn");

var jumlahPilihan = 0 ;


function pilihan_com(){
    const pilihan = [batu_com,kertas_com,gunting_com];
    const perbandingan = ['batu', 'kertas', 'gunting'];
    const hasil_com = Math.floor(Math.random()*3);
     pilihan_computer = pilihan[hasil_com];
     nama_pilihan_com = perbandingan[hasil_com];
    return pilihan_computer, nama_pilihan_com
}

function main (){
    

    batu_user.addEventListener('click', function(){
        pilihan_com();
        start('batu', nama_pilihan_com);
    })

    kertas_user.addEventListener('click', function(){
        pilihan_com();
        start('kertas', nama_pilihan_com);
    })

    gunting_user.addEventListener('click', function(){
        pilihan_com();
        start('gunting', nama_pilihan_com);
    })

    refresh_button.addEventListener('click', function(){
        document.location.reload();
    })

}


function seri(){
    document.getElementById('hasil').src = "assets/draw.png";
    jumlahPilihan++;
    console.log('Hasil yang di dapat seri');
}

function menang(){
    document.getElementById('hasil').src = "assets/player-1win.png";
    jumlahPilihan++;
    console.log('Hasil yang di dapat user menang');
}

function kalah(){
    document.getElementById('hasil').src = "assets/com-win.png";
    jumlahPilihan++;
    console.log('Hasil yang di dapat komputer menang');
}


function start(pilihan_user, nama_pilihan_com){
    
    if (jumlahPilihan === 0){

            // Cek jika seri
        if (pilihan_user === nama_pilihan_com){  
            console.log(`Pilihan komputer adalah ${nama_pilihan_com}`)
            console.log(`Pilihan user adalah ${pilihan_user}`);    
     
            seri();
            return;
        }  

        // Cek jika user milih batu
        if (pilihan_user ==='batu'){
            console.log(`Pilihan komputer adalah ${nama_pilihan_com}`)
            console.log('Pilihan user adalah batu');    
            if(nama_pilihan_com === 'kertas'){
                kalah();
                return;
            } else {
                menang();
                return;
            }
        }

        // Cek jika user milih gunting
        if (pilihan_user ==='gunting'){
            console.log(`Pilihan komputer adalah ${nama_pilihan_com}`)
            console.log('Pilihan user adalah gunting');    
            if(nama_pilihan_com === 'kertas'){
                menang();
                return;
            } else {
                kalah();
                return;
            }
        }

        // Cek jika user milih kertas
        if (pilihan_user ==='kertas'){
            console.log(`Pilihan komputer adalah ${nama_pilihan_com}`)
            console.log('Pilihan user adalah kertas');
    
            if(nama_pilihan_com === 'batu'){
                menang();
                return;
            } else {
                kalah();
                return;
            }
        }


    } else {
        
        console.log ('Tekan Tombol refresh untuk bermain lagi');

    }

    
    
}






main();